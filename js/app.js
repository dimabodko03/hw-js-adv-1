class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get nameInfo() {
        return this._name;
    }

    set name(newName) {
        this._name = newName;
    }

    get ageInfo() {
        return this._age;
    }

    set age(newAge) {
        this._age = newAge;
    }

    get salaryInfo() {
        return this._salary;
    }

    set salary(newSalary) {
        this._salary = newSalary;
    }
}



class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this._lang = lang;
    }

    get salary() {
        return this._salary * 3;
    }

    get lang() {
        return this._lang;
    }

    set lang(newLang) {
        this._lang = newLang;
    }
}

const prog1 = new Programmer("Dima", 20, 40000, ["JavaScript", "Python"]);
const prog2 = new Programmer("Masha", 24, 50000, ["Java", "C++"]);

console.log("Programmer 1:", prog1);
console.log("Programmer 2:", prog2);
